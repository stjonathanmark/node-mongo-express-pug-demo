const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');

const itemCtrl = require('./server/controllers/item');
const personCtrl = require('./server/controllers/person');
const defaultCtrl = require('./server/controllers/default');

const app = express();

app.use(express.static(path.join(__dirname, 'public')));

app.set('view engine', 'pug');
app.set('views', path.join(__dirname, 'server/views'));

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, Origin, X-Requested-With, Content-Type, Accept');
    res.header('Access-Control-Allow-Methods', 'DELETE, GET, PATCH, POST, PUT')
    next();
});

app.use('/item', itemCtrl);
app.use('/person', personCtrl);
app.use('/', defaultCtrl);

app.listen(3000, () => {
    console.log('Listening at http://localhost:3000');
});

