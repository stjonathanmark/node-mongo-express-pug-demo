# Description
This is a demo app that is created with Node, MongoDb, Express, and Pug.

# Instructions
1. To run this application you will need to download and install **NodeJs** and a **MongoDB**. [Download NodeJs](https://nodejs.org) - [Download MongoDB](https://www.mongodb.org/)
2. You need to create database named **'appdata'** on the MongoDB server and then create two collections in that database named **'people'** and **'items'**
3. Modify the **'url'** variable to point in to the correct MongoDB server on **line 6** in the ['data-source.js' code file](https://bitbucket.org/stjonathanmark/node-mongo-express-pug-demo/src/master/server/data/data-source.js) located in the **'server/data'** folder
4. Open the NodeJs command line tool and set it's path to the repository folder for the demo app (a. k. a. Application Root) where ever you placed it on your computer after download
5. Type and run the command **'npm install'** in the NodeJs command line tool to install all the dependencies from the **'package.json'** file
6. Type and run the command **'node app'** in the NodeJs command line tool to start node server hosting the website
7. Open any browser and type ['http://localhost:3000']('http://localhost:3000') in the address bar and press enter
8. Enjoy!!!!!!!