const mongoose = require('mongoose');

const Person = require('../models/person');
const Item = require('../models/item');

const url = 'mongodb://localhost/appdata';

mongoose.connect(url, { useNewUrlParser: true });

class DataSource {
    // #region Person Data Operations

    async getPeople() {
        return await Person.find();
    }

    async getPerson(id) {
        return await Person.findById(id);
    }

    async addPerson(person) {
        let schema = new Person(person);
        return await schema.save();
    }

    async updatePerson(person) {
        return await Person.updateOne({ _id: person._id }, person);
    }

    async deletePerson(id) {
        await Item.deleteMany({ person: id });
        return await Person.deleteOne({ _id: id });
    }

    // #endregion

    // #region Item Data Operations

    async getItems() {
        let items = await Item.find();
        await Person.populate(items, { path: 'person', model: 'Person' });
        return items;
    }

    async getItem(id) {
        let item = await Item.findById(id);
        await Person.populate(item, { path: 'person', model: 'Person' });
        return item;
    }

    async getPersonItems(personId) {
        return await Item.find({ person: personId });
    }

    async addItem(item) {
        let schema = new Item(item);
        return await schema.save();
    }

    async updatItem(item) {
        return await Item.updateOne({ _id: item._id }, item);
    }

    async deleteItem(id) {
        return await Item.deleteOne({ _id: id });
    }

    // #endregion
}

module.exports = new DataSource();
