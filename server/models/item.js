const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const itemSchema = new Schema({
    name: String,
    person: {
        type: Schema.Types.ObjectId,
        ref: 'Person'
    }
});

module.exports = mongoose.model('Item', itemSchema, 'items');