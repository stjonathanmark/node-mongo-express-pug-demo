const express = require('express');

const router = express.Router();

router.route('/')
    .get((req, res) => {
        res.render('default/index', { title: 'Welcome to the People\'s App' });
    });

module.exports = router;