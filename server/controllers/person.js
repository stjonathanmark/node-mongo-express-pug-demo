const express = require('express');

const data = require('../data/data-source');

const router = express.Router();

router.route('/')
    .get(async (req, res) => {
        let viewName = 'person/index';
        let viewModel = {
            title: 'Behold the People',
            people: [],
            message: '',
            hasError: false
        }

        try {
            viewModel.people = await data.getPeople();
            if (req.query.err) {
                viewModel.hasError = true;
                let errMsg = '';
                switch (req.query.err) {
                    case 'update':
                        errMsg = 'An error occurred getting person to update';
                        break;
                    case 'updating':
                        errMsg = 'An error occurred updating person';
                        break;
                    case 'creating':
                        errMsg = 'An error occurred creating person';
                        break;
                    case 'deleting':
                        errMsg = 'An error occurred deleting person';
                        break;
                    case 'get-items':
                        errMsg = 'An error occurred getting person\'s items';
                        break;
                    default:
                        errMsg = 'An error occurred';
                }

                viewModel.message = errMsg;
            }
            res.render(viewName, viewModel);
        } catch (err) {
            res.render(viewName, { message: 'An error occured retrieving people', ...viewModel });
        }
    });

router.route('/create')
    .get((req, res) => {
        res.render('person/create', { title: 'Create New Person' });
    })
    .post(async (req, res) => {
        try {
            await data.addPerson(req.body);
            res.redirect('/person');
        } catch (err) {
            res.redirect('/person?err=creating');
        }
    });

router.route('/update/:id')
    .get(async (req, res) => {
        try {
            let person = await data.getPerson(req.params.id);
            res.render('person/update', { title: 'Update Person', person: person });
        } catch (err) {
            res.redirect('/person?err=update');
        }
    })
    .post(async (req, res) => {
        try {
            await data.updatePerson({ _id: req.params.id, ...req.body });
            res.redirect('/person');
        } catch (err) {
            res.redirect('/person?err=updating');
        }
    });;

router.route('/delete/:id')
    .get(async (req, res) => {
        try {
            await data.deletePerson(req.params.id);
            res.redirect('/person');
        } catch (err) {
            res.redirect('/person?err=deleting');
        }
    });

router.route('/:id/item')
    .get(async (req, res) => {
        try {
            let hasError = false;
            let errMsg = '';
            if (req.query.err) {
                hasError = true;
                switch (req.query.err) {
                    case 'update':
                        errMsg = 'An error occurred getting item to update';
                        break;
                    case 'updating':
                        errMsg = 'An error occurred updating item';
                        break;
                    case 'creating':
                        errMsg = 'An error occurred accessing create item form';
                        break;
                    case 'creating':
                        errMsg = 'An error occurred creating item';
                        break;
                    case 'deleting':
                        errMsg = 'An error occurred deleting item';
                        break;
                    default:
                        errMsg = 'An error occurred';
                }
            }

            let personId = req.params.id;
            let person = await data.getPerson(personId);
            let items = await data.getPersonItems(personId);
            res.render('person/items', { title: `${person.fullName}'s Items`, person: person, items: items, hasError: hasError, message: errMsg });
        } catch (err) {
            console.error(err);
            res.redirect('/person?err=get-items');
        }
    });

router.route('/:id/item/create')
    .get(async (req, res) => {
        try {
            let person = await data.getPerson(req.params.id);
            res.render('person/create-item', { title: 'Create New Item', person: person, message: `Creating Item for ${person.fullName}` });
        } catch (err) {
            res.redirect(`/person/${req.params.id}/item?err=create`)
        }
    })
    .post(async (req, res) => {
        try {
            let item = req.body;
            item.person = req.params.id;
            await data.addItem(item);
            res.redirect(`/person/${req.params.id}/item`)
        } catch (err) {
            res.redirect(`/person/${req.params.id}/item?err=creating`);
        }
    });

router.route('/:id/item/update/:itemId')
    .get(async (req, res) => {
        try {
            let item = await data.getItem(req.params.itemId);
            res.render('person/update-item', { item: item, message: `Updating Item for ${item.person.fullName}` });
        } catch (err) {
            res.redirect(`/person/${req.params.id}/item?err=update`);
        }
    })
    .post(async (req, res) => {
        try {
            let item = req.body;
            item._id = req.params.itemId;
            item.person = req.params.id;
            await data.updatItem(item);
            res.redirect(`/person/${req.params.id}/item`);
        } catch (err) {
            res.redirect(`/person/${req.params.id}/item?err=updating`);
        }
    });

router.route('/:id/item/delete/:itemId')
    .get(async (req, res) => {
        try {
            await data.deleteItem(req.params.itemId);
            res.redirect(`/person/${req.params.id}/item`);
        } catch (err) {
            res.redirect(`/person/${req.params.id}/item?err=deleting`);
        }
    });

module.exports = router;