const express = require('express');

const data = require('../data/data-source');

const router = express.Router();

router.route('/')
    .get(async (req, res) => {
        let viewName = 'item/index';
        let viewModel = {
            title: 'Behold the People\'s Stuff',
            items: [],
            message: '',
            hasError: false
        }

        try {
            viewModel.items = await data.getItems();
            if (req.query.err) {
                viewModel.hasError = true;
                let errMsg = '';
                switch (req.query.err) {
                    case 'create':
                        errMsg = 'An error occurred getting create form';
                        break;
                    case 'creating':
                        errMsg = 'An error occurred creating item';
                        break;
                    case 'deleting':
                        errMsg = 'An error occurred deleting item';
                        break;
                    case 'update':
                        errMsg = 'An error occurred getting item to update';
                        break;
                    case 'updating':
                        errMsg = 'An error occurred updating item';
                        break;
                    default:
                        errMsg = 'An error occurred';
                }

                viewModel.message = errMsg;
            }
            res.render(viewName, viewModel);
        } catch (err) {
            res.render(viewName, { message: 'An error occured retrieving items', ...viewModel });
        }
    });

router.route('/create')
    .get(async (req, res) => {
        try {
            let people = await data.getPeople();
            res.render('item/create', { title: 'Create New Item', people: people });
        } catch (err) {
            res.redirect('/item?err=create');
        }
    })
    .post(async (req, res) => {
        try {
            let item = req.body;
            await data.addItem(item);
            res.redirect('/item');
        } catch (err) {
            res.redirect('/item?err=creating');
        }
    });

router.route('/update/:id')
    .get(async (req, res) => {
        try {
            let people = await data.getPeople();
            let item = await data.getItem(req.params.id);
            res.render('item/update', { title: 'Update Item', item: item, people: people });
        } catch (err) {
            res.redirect('/item?err=update');
        }
    })
    .post(async (req, res) => {
        try {
            let item = req.body;
            item._id = req.params.id;
            await data.updatItem(item);
            res.redirect('/item');
        } catch (err) {
            res.redirect('/item?err=updating');
        }
    });

router.route('/delete/:id')
    .get(async (req, res) => {
        try {
            await data.deleteItem(req.params.id);
            res.redirect('/item');
        } catch (err) {
            res.redirect('/item?err=deleting');
        }
    });

module.exports = router;